#!/bin/bash
#
#SBATCH --job-name=bios823hw3
#SBATCH --mail-user=
#SBATCH --mail-type=END,FAIL
#SBATCH --mem=4096
#SBATCH --time=03:00:00
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=1
#SBATCH --output=bios823hw3-%J.stdout
#SBATCH --error=bios823hw3-%J.stderr

mysif=/work/owzar001/singularity/bios823container.sif


singularity exec \
  ${mysif} \
  Rscript -e \
  "rmarkdown::render('HW3_Sun_Scott.Rmd')"
